<?php
return [
    'modules' => [
        'DoctrineModule',
        'DoctrineORMModule',
        'ZF\Apigility',
        'ZF\Apigility\Provider',
        'AssetManager',
        'ZF\ApiProblem',
        'ZF\MvcAuth',
        'ZF\OAuth2',
        'ZF\Hal',
        'ZF\ContentNegotiation',
        'ZF\ContentValidation',
        'ZF\Rest',
        'ZF\Rpc',
        'ZF\Versioning',
        'ZF\DevelopmentMode',
        'Phpro\DoctrineHydrationModule',
        'ZF\Apigility\Doctrine\Server',
        'ZF\Apigility\Documentation',
        'ZF\Apigility\Documentation\Swagger',
        'ZF\OAuth2\Doctrine',
        'ZF\Configuration',
        'Db',
        'API',
    ],
    'module_listener_options' => [
        'module_paths' => [
            './vendor',
        ],
        'config_glob_paths' => [
            __DIR__ . sprintf('/autoload/{,*.}{global,%s,local}.php', ENV)
        ],
        'config_cache_enabled' => false,
    ]
];
