<?php
return [
    'zf-oauth2' => [
        'storage' => 'oauth2.doctrineadapter.default',
        'allow_implicit' => false,
        'access_lifetime' => 3600,
        'enforce_state' => true,
        'grant_types' => [
            'client_credentials' => false,
            'authorization_code' => true,
            'password' => true,
            'refresh_token' => true,
            'jwt' => false
        ],
        'options' => [
            'always_issue_new_refresh_token' => true,
        ]
    ],
];
