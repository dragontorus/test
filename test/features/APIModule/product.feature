@role @api
Feature: Product CRUD
  As a user,
  I should be able to perform CRUD operations over product entities

  Background:
    Given database tables are empty
    And database table 'Client_OAuth2' contains the following records:
      |id|clientId |secret          |redirectUri            |
      |1 |worker|supersecretpassword|http://test.local:8081/|

  @get
  Scenario: Get product information
    And database table 'category' contains the following records:
      |id|parent |name         |description|
      |1 |null      |first        |desc1      |
      |2 |1         |sub_first    |desc_sub   |
    And database table 'product' contains the following records:
      |id|name      |description   |price      |quantity   |category   |
      |1 |product1  |prodDesc 1    |5          |3          |1          |
      |2 |product2  |prodDesc 2    |6          |2          |2          |
      |3 |product3  |prodDesc 3    |7          |1          |2          |
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|

    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|
    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
    When I make a 'GET' request to '/product/1'
    Then I should get '200' response code
    And response body should be a valid json
    And json response body should contain property 'id' equal to '1'
    And json response body should contain property 'name' equal to 'product1'
    And json response body should contain property 'description' equal to 'prodDesc 1'
    And json response body should contain property 'category_id' equal to '1'


  Scenario: Get product list
    And database table 'category' contains the following records:
      |id|parent |name         |description|
      |1 |null      |first        |desc1      |
      |2 |1         |sub_first    |desc_sub   |
    And database table 'product' contains the following records:
      |id|name      |description   |price      |quantity   |category   |
      |1 |product1  |prodDesc 1    |5          |3          |1          |
      |2 |product2  |prodDesc 2    |6          |2          |2          |
      |3 |product3  |prodDesc 3    |7          |1          |2          |
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|

    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|

    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
    When I make a 'GET' request to '/product'
    Then I should get '200' response code
    And response body should be a valid json
    And json response body should contain property '_embedded->products->0->id' equal to '1'
    And json response body should contain property '_embedded->products->1->id' equal to '2'
    And json response body should contain property '_embedded->products->2->id' equal to '3'
    And json response body should contain property '_embedded->products->0->name' equal to 'product1'
    And json response body should contain property '_embedded->products->1->name' equal to 'product2'
    And json response body should contain property '_embedded->products->2->name' equal to 'product3'
    And json response body should contain property '_embedded->products->0->category_id' equal to '1'
    And json response body should contain property '_embedded->products->1->category_id' equal to '2'
    And json response body should contain property '_embedded->products->2->category_id' equal to '2'


  Scenario: Get product list with sort
    And database table 'category' contains the following records:
      |id|parent |name         |description|
      |1 |null      |first        |desc1      |
      |2 |1         |sub_first    |desc_sub   |
    And database table 'product' contains the following records:
      |id|name      |description   |price      |quantity   |category   |
      |1 |product1  |prodDesc 1    |5          |3          |1          |
      |2 |product2  |prodDesc 2    |6          |2          |2          |
      |3 |product3  |prodDesc 3    |7          |1          |2          |
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|

    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|

    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
    When I make a 'GET' request to '/product?order=desc&field=id'
    Then I should get '200' response code
    And response body should be a valid json
    And json response body should contain property '_embedded->products->0->id' equal to '3'
    And json response body should contain property '_embedded->products->1->id' equal to '2'
    And json response body should contain property '_embedded->products->2->id' equal to '1'
    And json response body should contain property '_embedded->products->0->name' equal to 'product3'
    And json response body should contain property '_embedded->products->1->name' equal to 'product2'
    And json response body should contain property '_embedded->products->2->name' equal to 'product1'
    And json response body should contain property '_embedded->products->0->category_id' equal to '2'
    And json response body should contain property '_embedded->products->1->category_id' equal to '2'
    And json response body should contain property '_embedded->products->2->category_id' equal to '1'

  @create
  Scenario: Create product
    Given database table 'product' is empty
    And database table 'category' contains the following records:
      |id|parent    |name         |description|
      |1 |null      |first        |desc1      |

    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|

    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|

    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
      |Content-Type  |application/json|

    When I make a 'POST' request to '/product' with request body:
      """
      {
        "name": "product",
        "description": "productDesc",
        "price": "8",
        "quantity": "10",
        "category": "1"
      }
      """
    Then I should get '201' response code
    And response body should be a valid json
    And json response body should contain property 'id' equal to '1'
    And json response body should contain property 'name' equal to 'product'
    And json response body should contain property 'description' equal to 'productDesc'

  @create
  Scenario: Create product without authorization headers
    Given database table 'product' is empty
    And database table 'category' contains the following records:
      |id|parent    |name         |description|
      |1 |null      |first        |desc1      |

    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|

    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|

    And request headers:
      |header        |value                                          |
      |Accept        |application/json                               |
      |Content-Type  |application/json|

    When I make a 'POST' request to '/product' with request body:
      """
      {
        "name": "product",
        "description": "productDesc",
        "price": "8",
        "quantity": "10",
        "category": "1"
      }
      """
    Then I should get '403' response code
    And response body should be a valid json
    And json response body should contain property 'title' equal to 'Forbidden'
    And json response body should contain property 'detail' equal to 'Forbidden'


  @update
  Scenario: Update product
    Given database table 'product' contains the following records:
      |id|name      |description   |price      |quantity    |category   |
      |1 |product   |prodDesc      |8          |10          |1          |
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|
    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|
    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
      |Content-type  |application/json                               |
    When I make a 'PATCH' request to '/product/1' with request body:
      """
      {
        "name": "product1",
        "description": "productDesc1",
        "price": "8",
        "quantity": "10",
        "category": "1"
      }
      """
    Then I should get '200' response code
    And response body should be a valid json
    And json response body should contain property 'name' equal to 'product1'
    And json response body should contain property 'description' equal to 'productDesc1'


  @delete
  Scenario: Delete product
    Given database table 'product' contains the following records:
      |id|name      |description   |price      |quantity    |category   |
      |1 |product   |prodDesc      |8          |10          |1          |
      |2 |product2  |prodDesc 2    |8          |10          |1          |
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|
    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|
    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
      |Content-type  |application/json                               |
    When I make a 'DELETE' request to '/product/2'
    Then I should get '204' response code
    And database table 'product' should not contain the following records:
      |id|name      |description   |price      |quantity    |category   |
      |2 |product2   |prodDesc 2     |8          |10          |1          |