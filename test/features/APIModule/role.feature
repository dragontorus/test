@role @api
Feature: Role CRUD
  As a user,
  I should be able to perform CRUD operations over role entities

  Background:
    Given database tables are empty
    And database table 'Client_OAuth2' contains the following records:
      |id|clientId |secret             |redirectUri                           |
      |1 |worker|supersecretpassword|http://test.local:8081/|

  @get
  Scenario: Get role information
    And database table 'role' contains the following records:
      |id|roleId |parent|
      |1 |user   |null  |
      |2 |admin  |1     |
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|

    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|

    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
    When I make a 'GET' request to '/api/role/2'
    Then I should get '200' response code
    And response body should be a valid json
    And json response body should contain property 'id' equal to '2'
    And json response body should contain property 'roleId' equal to 'admin'


  @create
  Scenario: Create role
    Given database table 'role' is empty
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|

    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|

    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
      |Content-Type  |application/json|

    When I make a 'POST' request to '/api/role' with request body:
      """
      {
        "roleId": "editor"
      }
      """
    Then I should get '201' response code
    And json response body should contain property 'id' equal to '1'
    And json response body should contain property 'roleId' equal to 'editor'
    And database table 'role' should contain the following records:
      |id|roleId      |
      |1 |editor      |

  @update
  Scenario: Update role
    Given database table 'role' contains the following records:
      |id|roleId |
      |1 |user   |
      |2 |editor |
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|
    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|
    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
      |Content-type  |application/json                               |
    When I make a 'PATCH' request to '/api/role/2' with request body:
      """
      {
        "roleId": "supereditor",
        "parent": "1"
      }
      """
    Then I should get '200' response code
    And response body should be a valid json
    And json response body should contain property 'id' equal to '2'
    And json response body should contain property 'roleId' equal to 'supereditor'
    And json response body should contain property '_embedded->parent->roleId' equal to 'user'
    And json response body should contain property '_embedded->parent->id' equal to '1'
    And database table 'role' should contain the following records:
      |id|roleId      |parent|
      |2 |supereditor |1     |

  @delete
  Scenario: Delete role
    Given database table 'role' contains the following records:
      |id|roleId|
      |1 |admin |
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|
    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|
    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
      |Content-type  |application/json                               |
    When I make a 'DELETE' request to '/api/role/1'
    Then I should get '204' response code
    And database table 'role' should not contain the following records:
      |id|roleId|
      |1 |admin |


