@role @api
Feature: Category CRUD
  As a user,
  I should be able to perform CRUD operations over category entities

  Background:
    Given database tables are empty
    And database table 'Client_OAuth2' contains the following records:
      |id|clientId |secret          |redirectUri            |
      |1 |worker|supersecretpassword|http://test.local:8081/|

  @get
  Scenario: Get category information
    And database table 'category' contains the following records:
      |id|parent |name         |description|
      |1 |null      |first        |desc1      |
      |2 |1         |sub_first    |desc_sub   |
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|

    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|

    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
    When I make a 'GET' request to '/category/1'
    Then I should get '200' response code
    And response body should be a valid json
    And json response body should contain property 'id' equal to '1'
    And json response body should contain property 'name' equal to 'first'
    And json response body should contain property 'description' equal to 'desc1'

  @get
  Scenario: Get category information with products
    And database table 'category' contains the following records:
      |id|parent |name         |description|
      |1 |null      |first        |desc1      |
      |2 |1         |sub_first    |desc_sub   |
    And database table 'product' contains the following records:
      |id|name      |description   |price      |quantity   |category   |
      |1 |product1  |prodDesc 1    |5          |3          |1          |
      |2 |product2  |prodDesc 2    |6          |2          |2          |
      |3 |product3  |prodDesc 3    |7          |1          |2          |
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|

    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|

    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
    When I make a 'GET' request to '/category/2'
    Then I should get '200' response code
    And response body should be a valid json
    And json response body should contain property 'id' equal to '2'
    And json response body should contain property 'name' equal to 'sub_first'
    And json response body should contain property 'description' equal to 'desc_sub'
    And json response body should contain property 'products->0->id' equal to '2'
    And json response body should contain property 'products->1->id' equal to '3'
    And json response body should contain property 'parent->products->0->id' equal to '1'

  Scenario: Get category list
    And database table 'category' contains the following records:
      |id|parent |name         |description|
      |1 |null      |first        |desc1      |
      |2 |1         |sub_first    |desc_sub   |
    And database table 'product' contains the following records:
      |id|name      |description   |price      |quantity   |category   |
      |1 |product1  |prodDesc 1    |5          |3          |1          |
      |2 |product2  |prodDesc 2    |6          |2          |2          |
      |3 |product3  |prodDesc 3    |7          |1          |2          |
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|

    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|

    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
    When I make a 'GET' request to '/category'
    Then I should get '200' response code
    And response body should be a valid json
    And json response body should contain property '_embedded->categories->0->id' equal to '1'
    And json response body should contain property '_embedded->categories->1->id' equal to '2'
    And json response body should contain property '_embedded->categories->0->name' equal to 'first'
    And json response body should contain property '_embedded->categories->1->name' equal to 'sub_first'
    And json response body should contain property '_embedded->categories->0->products->0->id' equal to '1'
    And json response body should contain property '_embedded->categories->1->products->1->id' equal to '3'

  Scenario: Get category list with sort
    And database table 'category' contains the following records:
      |id|parent |name         |description|
      |1 |null      |first        |desc1      |
      |2 |1         |sub_first    |desc_sub   |
    And database table 'product' contains the following records:
      |id|name      |description   |price      |quantity   |category   |
      |1 |product1  |prodDesc 1    |5          |3          |1          |
      |2 |product2  |prodDesc 2    |6          |2          |2          |
      |3 |product3  |prodDesc 3    |7          |1          |2          |
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|

    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|

    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
    When I make a 'GET' request to '/category?order=desc&field=id'
    Then I should get '200' response code
    And response body should be a valid json
    And json response body should contain property '_embedded->categories->0->id' equal to '2'
    And json response body should contain property '_embedded->categories->1->id' equal to '1'
    And json response body should contain property '_embedded->categories->0->name' equal to 'sub_first'
    And json response body should contain property '_embedded->categories->1->name' equal to 'first'
    And json response body should contain property '_embedded->categories->0->products->1->id' equal to '3'
    And json response body should contain property '_embedded->categories->1->products->0->id' equal to '1'

  @create
  Scenario: Create category
    Given database table 'category' is empty
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|

    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|

    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
      |Content-Type  |application/json|

    When I make a 'POST' request to '/category' with request body:
      """
        {
          "name": "test",
          "description": "testdesc"
        }
      """
    Then I should get '201' response code
    And response body should be a valid json
    And json response body should contain property 'id' equal to '1'
    And json response body should contain property 'name' equal to 'test'
    And json response body should contain property 'description' equal to 'testdesc'
    And database table 'category' should contain the following records:
      |id|name         |description|
      |1 |test         |testdesc  |

  @create
  Scenario: Create category with parent
    Given database table 'category' contains the following records:
      |id|parent    |name         |description|
      |1 |null      |test         |testdesc   |
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|

    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|

    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
      |Content-Type  |application/json|

    When I make a 'POST' request to '/category' with request body:
      """
        {
          "name": "testsub",
          "description": "testdesc2",
          "parent": "1"
        }
      """
    Then I should get '201' response code
    And json response body should contain property 'id' equal to '2'
    And json response body should contain property 'parent->id' equal to '1'
    And json response body should contain property 'name' equal to 'testsub'
    And json response body should contain property 'description' equal to 'testdesc2'

  @create
  Scenario: Create category without authorization headers
    Given database table 'category' is empty
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|

    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|

    And request headers:
      |header        |value                                          |
      |Accept        |application/json                               |
      |Content-Type  |application/json|

    When I make a 'POST' request to '/category' with request body:
      """
        {
          "name": "test",
          "description": "testdesc"
        }
      """
    Then I should get '403' response code
    And response body should be a valid json
    And json response body should contain property 'title' equal to 'Forbidden'
    And json response body should contain property 'detail' equal to 'Forbidden'

  @update
  Scenario: Update category
    Given database table 'category' contains the following records:
      |id|parent    |name         |description|
      |1 |null      |test         |testdesc  |
      |2 |1         |test2        |testupdated |
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|
    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|
    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
      |Content-type  |application/json                               |
    When I make a 'PATCH' request to '/category/2' with request body:
      """
      {
          "name": "testUpd",
          "description": "testupdated"
      }
      """
    Then I should get '200' response code
    And response body should be a valid json
    And json response body should contain property 'id' equal to '2'
    And json response body should contain property 'name' equal to 'testUpd'
    And json response body should contain property 'description' equal to 'testupdated'
    And database table 'category' contains the following records:
      |id|parent    |name         |description |
      |1 |null      |test         |testdesc   |
      |2 |1         |test2        |testupdated |

  @delete
  Scenario: Delete category
    Given database table 'category' contains the following records:
      |id|name         |description|
      |1 |test         |test_desc  |
      |2 |test2        |testdesc2  |
    And database table 'user' contains the following records:
      |id|email                     |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com         |John     |Doe     |qwerty1234|active|
    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|
    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
      |Content-type  |application/json                               |
    When I make a 'DELETE' request to '/category/2'
    Then I should get '204' response code
    And database table 'category' should not contain the following records:
      |id|name         |description|
      |2 |test2        |testdesc2  |