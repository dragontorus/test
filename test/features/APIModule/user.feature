@user @api
Feature: User CRUD
  As a user,
  I should be able to perform CRUD operations over user entities

  Background:
    Given database tables are empty
    And database table 'Client_OAuth2' contains the following records:
      |id|clientId |secret             |redirectUri                           |
      |1 |worker|supersecretpassword|http://test.local:8081/|
    And database table 'acl_resource' contains the following records:
      |id|module|controller               |action   |resourceName             |
      |1 |      |ZF\OAuth2\Controller\Auth|authorize|OAuth2 authorization page|
      |2 |Auth  |Auth\Controller\User     |profile  |Profile page             |
      |3 |Auth	|Auth\Controller\User     |logout   |Logout action            |
    And database table 'role' contains the following records:
      |id|roleId |parent|
      |1 |user   |null  |
      |2 |admin  |1     |
      |3 |editor |2     |

  @create
  Scenario: Create user
    Given request headers:
      |header        |value           |
      |Accept        |application/json|
      |Content-Type  |application/json|
    When I make a 'POST' request to '/api/user' with request body:
      """
      {
        "email": "johndoe@gmail.com",
        "firstName": "John",
        "lastName": "Doe",
        "status": "active",
        "password": "qwerty1234",
        "roles": ["user","admin"]
      }
      """
    Then I should get '201' response code
    And response body should be a valid json
    And json response body should contain property 'id' equal to '1'
    And json response body should contain property 'email' equal to 'johndoe@gmail.com'
    And json response body should contain property 'firstName' equal to 'John'
    And json response body should contain property 'lastName' equal to 'Doe'
    And json response body should contain property 'status' equal to 'active'
    And json response body should contain property '_embedded->roles->0->id' equal to '1'
    And json response body should contain property '_embedded->roles->0->roleId' equal to 'user'
    And json response body should contain property '_embedded->roles->1->id' equal to '2'
    And json response body should contain property '_embedded->roles->1->roleId' equal to 'admin'
    And database table 'user' should contain the following records:
      |id|email            |firstName|lastName|status  |
      |1 |johndoe@gmail.com|John     |Doe     |active  |

  @get
  Scenario: Get user
    Given database table 'user' contains the following records:
      |id|email            |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com|John     |Doe     |qwerty1234|active|
    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|
    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |

    When I make a 'GET' request to '/api/user/1'
    Then I should get '200' response code
    And response body should be a valid json
    And json response body should contain property 'id' equal to '1'
    And json response body should contain property 'email' equal to 'johndoe@gmail.com'
    And json response body should contain property 'firstName' equal to 'John'
    And json response body should contain property 'lastName' equal to 'Doe'
    And json response body should contain property 'status' equal to 'active'



  @update
  Scenario: Update user
    Given database table 'user' contains the following records:
      |id|email            |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com|John     |Doe     |qwerty1234|active|
    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|
    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
      |Content-Type  |application/json                               |
    When I make a 'PATCH' request to '/api/user/1' with request body:
      """
      {
        "email": "mark.andersen@gmail.com",
        "firstName": "Mark",
        "lastName": "Andersen",
        "status": "disabled",
        "password": "1234qwerty",
        "roles": ["admin"]
      }
      """
    Then I should get '200' response code
    And response body should be a valid json
    And json response body should contain property 'id' equal to '1'
    And json response body should contain property 'email' equal to 'mark.andersen@gmail.com'
    And json response body should contain property 'firstName' equal to 'Mark'
    And json response body should contain property 'lastName' equal to 'Andersen'
    And json response body should contain property 'status' equal to 'disabled'
    And json response body should contain property '_embedded->roles->0->id' equal to '2'

  @delete
  Scenario: Delete user
    Given database table 'user' contains the following records:
      |id|email            |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com|John     |Doe     |qwerty1234|active|
    And database table 'AccessToken_OAuth2' contains the following records:
      |id|client|user|accessToken                             |expires            |
      |1 |1     |1   |a04b79cb76b18113fe540e86e6755925a8d67d7b|2070-12-12 00:00:00|
    And request headers:
      |header        |value                                          |
      |Authorization |Bearer a04b79cb76b18113fe540e86e6755925a8d67d7b|
      |Accept        |application/json                               |
      |Content-Type  |application/json                               |
    When I make a 'DELETE' request to '/api/user/1'
    Then I should get '204' response code
    And database table 'user' should not contain the following records:
      |id|email            |firstName|lastName|password  |status|
      |1 |johndoe@gmail.com|John     |Doe     |qwerty1234|active|
