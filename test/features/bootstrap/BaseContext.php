<?php

use Behat\MinkExtension\Context\MinkContext;
use Behat\Testwork\Hook\Scope\SuiteScope;
use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\Mink\Driver\Selenium2Driver;
use Behat\Gherkin\Node\TableNode;
use Db\Service\User as UserService;

require __DIR__ . '/../../../vendor/autoload.php';
require __DIR__ . '/../../../vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';

abstract class BaseContext extends MinkContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */

    protected $application;

    public function __construct()
    {
        define('ENV', 'test');
        $this->application = \Zend\Mvc\Application::init(require __DIR__ . '/../../../config/application.config.php');
    }


    /**
     * Take screenshot when step fails.
     * Works only with Selenium2Driver.
     *
     * @AfterStep
     */
    public function takeScreenshotAfterFailedStep(AfterStepScope $scope) {
        if (!$scope->getTestResult()->isPassed() && $this->getSession()->getDriver() instanceof Selenium2Driver) {

            $screenshotPath = "/tmp/selenium/";

            $featureName = pathinfo($scope->getFeature()->getFile(), PATHINFO_FILENAME);

            $screenshotPath .= $featureName;

            if(!file_exists($screenshotPath)) {
                mkdir($screenshotPath, 0777, true);
            }


            $screenShotName = sprintf('%s_%s_%s.%s', "Line" . $scope->getStep()->getLine(), $this->getMinkParameter('browser_name'), date('c'), 'png');

            $this->saveScreenshot($screenShotName, $screenshotPath);
        }
    }

    /**
     * Start up the web server
     *
     * @BeforeSuite
     */
    public static function setUp(SuiteScope $scope) {
        $webserverParams = $scope->getEnvironment()->getSuite()->getSetting('webserver');

        if (!self::canConnectToHttpd($webserverParams['host'], $webserverParams['port'])) {
            self::startBuiltInHttpd($webserverParams['host'], $webserverParams['port'], realpath(__DIR__ . '/../../../public/'));
        }

    }

    /**
     * Kill the httpd process if it has been started when the tests have finished
     *
     * @AfterSuite
     */
    public static function tearDown(SuiteScope $scope) {
        $webserverParams = $scope->getEnvironment()->getSuite()->getSetting('webserver');
        self::killHttpServer($webserverParams['host'], $webserverParams['port'], realpath(__DIR__ . '/../../../public/'));
    }

    /**
     * Kills HTTP web server
     *
     * @param $host
     * @param $port
     * @param $documentRoot
     */
    private static function killHttpServer($host, $port, $documentRoot) {
        $command = sprintf('pkill -f "php -d variables_order=EGPCS -S %s:%d -t %s %s/index.php"', $host, $port, $documentRoot, $documentRoot);
        exec($command);
    }

    /**
     * Checks if it is possible to connect to running web server
     *
     * @param string $host The hostname to connect to
     * @param int $port The port to use
     * @return boolean
     */
    private static function canConnectToHttpd($host, $port) {
        // Disable error handler for now
        set_error_handler(function() { return true; });
        // Try to open a connection
        $sp = fsockopen($host, $port);

        // Restore the handler
        restore_error_handler();

        if ($sp === false) {
            return false;
        }
        fclose($sp);
        return true;
    }

    /**
     * Start the built in httpd
     *
     * @param string $host The hostname to use
     * @param int $port The port to use
     * @param string $documentRoot The document root
     * @return int Returns the PID of the httpd
     */
    private static function startBuiltInHttpd($host, $port, $documentRoot) {
        // Build the command
        $command = sprintf('ENV=test php -d variables_order=EGPCS -S %s:%d -t %s %s/index.php > /dev/null 2>&1 & echo $!', $host, $port, $documentRoot, $documentRoot);
        $output = array();
        exec($command, $output);
        return (int) $output[0];
    }

    /**
     * @Given database table :arg1 contains the following records:
     * @Given database table :arg1 is empty
     */
    public function databaseTableContainsTheFollowingRecords($tableName, TableNode $records = NULL)
    {
        /**
         * @var $em \Doctrine\ORM\EntityManager
         */
        $em = $this->application->getServiceManager()->get('Doctrine\ORM\EntityManager');
        $dbConnection = $em->getConnection();
        $dbConnection->query('SET FOREIGN_KEY_CHECKS=0');
        $dbConnection->executeUpdate($dbConnection->getDatabasePlatform()->getTruncateTableSQL($tableName));
        $dbConnection->query('SET FOREIGN_KEY_CHECKS=1');

        if($records) {
            $entityClassName = NULL;
            foreach ($em->getMetadataFactory()->getAllMetadata() as $metaData) {
                if ($metaData->getTableName() == $tableName) {
                    $entityClassName = $metaData->getName();
                    break;
                }
            }

            assertNotNull($entityClassName);

            foreach($records->getHash() as $row){

                if($entityClassName == 'Db\Entity\User') {

                    /**
                     * @var $userService \Db\Service\User
                     */
                    $userService = $this->application->getServiceManager()->get('Db\Service\User');

                    if(isset($row['password'])) {
                        $row['salt'] = $userService->generateSalt();
                        $row['password'] = $userService->encryptPassword($row['password'], $row['salt']);
                    }

                    if(isset($row['roles']) && !empty($row['roles'])){
                        $row['roles'] = explode(',', $row['roles']);
                    }
                }

                if($entityClassName == 'ZF\OAuth2\Doctrine\Entity\Client') {
                    if(isset($row['secret'])) {
                        $bcrypt = new Zend\Crypt\Password\Bcrypt;
                        $row['secret'] = $bcrypt->create($row['secret']);
                    }
                }



                $hydrator = new \DoctrineModule\Stdlib\Hydrator\DoctrineObject($em, true);

                $entityInstance = new $entityClassName;
                $entityInstance = $hydrator->hydrate($row, $entityInstance);

                $em->persist($entityInstance);
                $em->flush($entityInstance);
            }

        }

    }

    /**
     * @Then database table :arg1 should contain the following records:
     */
    public function checkTableContainsRecords($tableName, TableNode $records)
    {
        /**
         * @var $em \Doctrine\ORM\EntityManager
         */
        $em = $this->application->getServiceManager()->get('Doctrine\ORM\EntityManager');

        if($records) {
            $entityClassName = NULL;
            foreach ($em->getMetadataFactory()->getAllMetadata() as $metaData) {
                if ($metaData->getTableName() == $tableName) {
                    $entityClassName = $metaData->getName();
                    break;
                }
            }

            assertNotNull($entityClassName);

            foreach ($records->getHash() as $row) {
                assertEquals(1, count($em->getRepository($entityClassName)->findBy($row)), "Table {$tableName} does not contain any record matching " . json_encode($row));
            }
        }
    }

    /**
     * @Given database tables are empty
     */
    public function truncateAllTables()
    {
        /**
         * @var $em \Doctrine\ORM\EntityManager
         */
        $em = $this->application->getServiceManager()->get('Doctrine\ORM\EntityManager');
        $dbConnection = $em->getConnection();

        $schemaManager = $dbConnection->getSchemaManager();
        $tables = $schemaManager->listTables();
        $query = [];
        $dbConnection->query('SET FOREIGN_KEY_CHECKS=0');
        foreach($tables as $table) {
            $name = $table->getName();
            $query[]= 'TRUNCATE ' . $name;
        }
        $dbConnection->executeQuery(implode(';', $query), array(), array());
        $dbConnection->query('SET FOREIGN_KEY_CHECKS=1');
    }

}