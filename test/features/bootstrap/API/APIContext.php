<?php


namespace API;


use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use GuzzleHttp\Client;

class APIContext extends \BaseContext implements Context, SnippetAcceptingContext
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;
    protected $requestHeaders = [];
    protected $requestBody = [];

    /**
     * @var \GuzzleHttp\Psr7\Response
     */
    protected $response;

    /**
     * @var string
     */
    protected $responseBody;

    public function __construct($baseUri)
    {
        parent::__construct();

        $this->client = new Client(
            [
                'base_uri' => $baseUri,
            ]
        );
    }

    /**
     * @Given request headers:
     */
    public function requestHeaders(TableNode $table)
    {
        $this->requestHeaders = [];
        foreach($table->getHash() as $row){
            $this->requestHeaders[$row['header']] = $row['value'];
        }
    }

    /**
     * @When I make a :arg1 request to :arg2
     * @When I make a :arg1 request to :arg2 with request body:
     */
    public function makeRequest($method, $uri, PyStringNode $body = NULL)
    {
        $this->response = $this->client->request(
            $method,
            $uri,
            [
                'http_errors' => false,
                'headers' => $this->requestHeaders,
                'body' => $body
            ]
        );

        $this->responseBody = $this->response->getBody()->getContents();
        //var_dump($this->responseBody);
    }

    /**
     * @Then I should get :arg1 response code
     */
    public function checkResponseCode($code)
    {
        assertEquals($code, $this->response->getStatusCode());
    }

    /**
     * @Then response body should be a valid json
     */
    public function checkJSONResponseBody()
    {
        assertJson($this->responseBody);
    }

    /**
     * @Then json response body should contain property :arg1 equal to :arg2
     */
    public function jsonResponseBodyShouldContainPropertyEqualTo($arg1, $arg2)
    {
        $responseData = json_decode($this->responseBody, true);
        $nestedProperties = explode('->', $arg1);
        $foundValue = $responseData;

        foreach($nestedProperties as $property) {
            assertArrayHasKey($property, $foundValue);
            $foundValue = $foundValue[$property];
        }
        assertEquals($arg2, $foundValue);
    }

    /**
     * @Then database table :arg1 should not contain the following records:
     */
    public function databaseTableShouldNotContainTheFollowingRecords($tableName, TableNode $records)
    {
        /**
         * @var $em \Doctrine\ORM\EntityManager
         */
        $em = $this->application->getServiceManager()->get('Doctrine\ORM\EntityManager');

        if($records) {
            $entityClassName = NULL;
            foreach ($em->getMetadataFactory()->getAllMetadata() as $metaData) {
                if ($metaData->getTableName() == $tableName) {
                    $entityClassName = $metaData->getName();
                    break;
                }
            }

            assertNotNull($entityClassName);

            foreach ($records->getHash() as $row) {
                assertEquals(0, count($em->getRepository($entityClassName)->findBy($row)), "Table {$tableName} does not contain any record matching " . json_encode($row));
            }
        }
    }


}