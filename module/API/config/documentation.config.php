<?php
return array(
    'API\\V1\\Rest\\User\\Controller' => array(
        'description' => 'Operations over users',
        'collection' => array(
            'description' => 'User list',
            'GET' => array(
                'description' => 'Get list of users',
                'response' => '{
   "email": "User email",
   "firstName": "User first name",
   "lastName": "User last name",
   "status": "User status(active/disabled)",
   "password": "User password. It should have more than 8 symbols",
   "company": "Company unique identifier(id)",
   "subscriptionPlan": "Subscription plan unique identifier(id)",
   "subscriptionPlanExpiresAt": "Subscription plan expiration date(format: \'Y-m-d H:i:s\')",
   "roles": "User roles, ex: [\'admin\', \'editor\']"
}',
            ),
            'DELETE' => array(
                'description' => 'Delete multiple users',
                'request' => '{
   "email": "",
   "firstName": "",
   "lastName": "",
   "status": ""
}',
                'response' => '{
   "email": "",
   "firstName": "",
   "lastName": "",
   "status": ""
}',
            ),
            'POST' => array(
                'request' => '{
   "email": "User email",
   "firstName": "User first name",
   "lastName": "User last name",
   "status": "User status(active/disabled)",
   "password": "User password. It should have more than 8 symbols",
   "company": "Company unique identifier(id)",
   "subscriptionPlan": "Subscription plan unique identifier(id)",
   "subscriptionPlanExpiresAt": "Subscription plan expiration date(format: \'Y-m-d H:i:s\')",
   "roles": "User roles, ex: [\'admin\', \'editor\']"
}',
                'description' => 'Create new user',
                'response' => '{
   "email": "User email",
   "firstName": "User first name",
   "lastName": "User last name",
   "status": "User status(active/disabled)",
   "password": "User password. It should have more than 8 symbols",
   "company": "Company unique identifier(id)",
   "subscriptionPlan": "Subscription plan unique identifier(id)",
   "subscriptionPlanExpiresAt": "Subscription plan expiration date(format: \'Y-m-d H:i:s\')",
   "roles": "User roles, ex: [\'admin\', \'editor\']"
}',
            ),
        ),
        'entity' => array(
            'description' => 'User entity',
            'GET' => array(
                'description' => 'Get user',
                'response' => '{
   "email": "User email",
   "firstName": "User first name",
   "lastName": "User last name",
   "status": "User status(active/disabled)",
   "password": "User password. It should have more than 8 symbols",
   "company": "Company unique identifier(id)",
   "subscriptionPlan": "Subscription plan unique identifier(id)",
   "subscriptionPlanExpiresAt": "Subscription plan expiration date(format: \'Y-m-d H:i:s\')",
   "roles": "User roles"
}',
            ),
            'PUT' => array(
                'description' => 'Update user',
                'request' => '{
   "email": "User email",
   "firstName": "User first name",
   "lastName": "User last name",
   "status": "User status(active/disabled)",
   "password": "User password. It should have more than 8 symbols",
   "company": "Company unique identifier(id)",
   "subscriptionPlan": "Subscription plan unique identifier(id)",
   "subscriptionPlanExpiresAt": "Subscription plan expiration date(format: \'Y-m-d H:i:s\')",
   "roles": "User roles, ex: [\'admin\', \'editor\']"
}',
                'response' => '{
   "email": "User email",
   "firstName": "User first name",
   "lastName": "User last name",
   "status": "User status(active/disabled)",
   "password": "User password. It should have more than 8 symbols",
   "company": "Company unique identifier(id)",
   "subscriptionPlan": "Subscription plan unique identifier(id)",
   "subscriptionPlanExpiresAt": "Subscription plan expiration date(format: \'Y-m-d H:i:s\')",
   "roles": "User roles, ex: [\'admin\', \'editor\']"
}',
            ),
            'DELETE' => array(
                'description' => 'Remove user',
                'request' => '{
   "email": "User email",
   "firstName": "User first name",
   "lastName": "User last name",
   "status": "User status(active/disabled)",
   "password": "User password. It should have more than 8 symbols",
   "company": "Company unique identifier(id)",
   "subscriptionPlan": "Subscription plan unique identifier(id)",
   "subscriptionPlanExpiresAt": "Subscription plan expiration date(format: \'Y-m-d H:i:s\')",
   "roles": "User roles, ex: [\'admin\', \'editor\']"
}',
                'response' => '{
   "email": "User email",
   "firstName": "User first name",
   "lastName": "User last name",
   "status": "User status(active/disabled)",
   "password": "User password. It should have more than 8 symbols",
   "company": "Company unique identifier(id)",
   "subscriptionPlan": "Subscription plan unique identifier(id)",
   "subscriptionPlanExpiresAt": "Subscription plan expiration date(format: \'Y-m-d H:i:s\')",
   "roles": "User roles, ex: [\'admin\', \'editor\']"
}',
            ),
            'POST' => array(
                'description' => 'Create user',
                'request' => '{
   "email": "",
   "firstName": "",
   "lastName": "",
   "status": ""
}',
                'response' => '{
   "email": "",
   "firstName": "",
   "lastName": "",
   "status": ""
}',
            ),
            'PATCH' => array(
                'description' => 'Update user(partial)',
                'request' => '{
   "email": "User email",
   "firstName": "User first name",
   "lastName": "User last name",
   "status": "User status(active/disabled)",
   "password": "User password. It should have more than 8 symbols",
   "company": "Company unique identifier(id)",
   "subscriptionPlan": "Subscription plan unique identifier(id)",
   "subscriptionPlanExpiresAt": "Subscription plan expiration date(format: \'Y-m-d H:i:s\')",
   "roles": "User roles, ex: [\'admin\', \'editor\']"
}',
                'response' => '{
   "email": "User email",
   "firstName": "User first name",
   "lastName": "User last name",
   "status": "User status(active/disabled)",
   "password": "User password. It should have more than 8 symbols",
   "company": "Company unique identifier(id)",
   "subscriptionPlan": "Subscription plan unique identifier(id)",
   "subscriptionPlanExpiresAt": "Subscription plan expiration date(format: \'Y-m-d H:i:s\')",
   "roles": "User roles"
}',
            ),
        ),
    ),
    'API\\V1\\Rest\\Role\\Controller' => array(
        'description' => 'Operations over system roles',
        'collection' => array(
            'description' => 'Get a list of  system roles',
            'GET' => array(
                'description' => 'Get a list of  system roles',
                'response' => '{
   "roleId": ""
}',
            ),
            'POST' => array(
                'description' => 'Create new role',
                'request' => '{
   "roleId": "",
   "parent": "Parent role unique identifier"
}',
                'response' => '{
   "roleId": ""
}',
            ),
        ),
        'entity' => array(
            'description' => 'Role',
            'GET' => array(
                'description' => 'Get a role by unique identifier',
                'response' => '{
   "roleId": ""
}',
            ),
            'PUT' => array(
                'request' => '{
   "roleId": ""
}',
            ),
            'DELETE' => array(
                'description' => 'Remove role',
                'request' => '{
   "roleId": "",
   "parent": "Parent role unique identifier"
}',
                'response' => '{
   "roleId": ""
}',
            ),
            'PATCH' => array(
                'description' => 'Update role(partial)',
                'request' => '{
   "roleId": "",
   "parent": "Parent role unique identifier"
}',
                'response' => '{
   "roleId": ""
}',
            ),
        ),
    ),
    'API\\V1\\Rest\\Client\\Controller' => array(
        'description' => 'Operations over oauth2 clients',
        'collection' => array(
            'description' => 'Operations over client lists',
            'GET' => array(
                'description' => 'Get a list of clients',
                'response' => '{
   "clientId": "",
   "redirectUri": "",
   "grantType": ""
}',
            ),
            'POST' => array(
                'description' => 'Create a new client',
                'request' => '{
   "clientId": "",
   "secret": "",
   "redirectUri": "",
   "grantType": ""
}',
                'response' => '{
   "clientId": "",
   "secret": "",
   "redirectUri": "",
   "grantType": ""
}',
            ),
        ),
        'entity' => array(
            'description' => 'Operations over a client',
            'GET' => array(
                'description' => 'Get client by id',
                'response' => '{
   "clientId": "",
   "secret": "",
   "redirectUri": "",
   "grantType": ""
}',
            ),
            'PATCH' => array(
                'description' => 'Update client(partially)',
                'request' => '{
   "clientId": "",
   "secret": "",
   "redirectUri": "",
   "grantType": ""
}',
                'response' => '{
   "clientId": "",
   "secret": "",
   "redirectUri": "",
   "grantType": ""
}',
            ),
            'DELETE' => array(
                'description' => 'Remove client',
                'request' => '{
   "clientId": "",
   "secret": "",
   "redirectUri": "",
   "grantType": ""
}',
                'response' => '{
   "clientId": "",
   "secret": "",
   "redirectUri": "",
   "grantType": ""
}',
            ),
        ),
    ),
    'API\\V1\\Rest\\Scope\\Controller' => array(
        'description' => 'Operations over oauth2 scopes',
        'collection' => array(
            'description' => 'Operation over scopes',
            'GET' => array(
                'description' => 'Get scope list',
                'response' => '{
   "scope": ""
}',
            ),
            'POST' => array(
                'description' => 'Create scope',
                'request' => '{
   "scope": ""
}',
                'response' => '{
   "scope": ""
}',
            ),
        ),
        'entity' => array(
            'description' => 'Operations over single scope',
            'GET' => array(
                'description' => 'Get scope by id',
                'response' => '{
   "scope": ""
}',
            ),
            'PATCH' => array(
                'description' => 'Update scope',
                'request' => '{
   "scope": ""
}',
                'response' => '{
   "scope": ""
}',
            ),
            'DELETE' => array(
                'description' => 'Remove scope',
                'request' => '{
   "scope": ""
}',
                'response' => '{
   "scope": ""
}',
            ),
        ),
    ),
);
