<?php
return array(
    'router' => array(
        'routes' => array(
            'api.rest.doctrine.user' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/user[/:user_id]',
                    'defaults' => array(
                        'controller' => 'API\\V1\\Rest\\User\\Controller',
                    ),
                ),
            ),
            'api.rest.doctrine.me' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/me',
                    'defaults' => array(
                        'user_id' => '',
                        'controller' => 'API\\V1\\Rest\\User\\Controller',
                    ),
                ),
            ),
            'api.rest.doctrine.role' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/role[/:role_id]',
                    'defaults' => array(
                        'controller' => 'API\\V1\\Rest\\Role\\Controller',
                    ),
                ),
            ),
            'api.rest.doctrine.client' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/client[/:client_id]',
                    'defaults' => array(
                        'controller' => 'API\\V1\\Rest\\Client\\Controller',
                    ),
                ),
            ),
            'api.rest.product' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/product[/:product_id]',
                    'defaults' => array(
                        'controller' => 'API\\V1\\Rest\\Product\\Controller',
                    ),
                ),
            ),
            'api.rest.category' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/category[/:category_id]',
                    'defaults' => array(
                        'controller' => 'API\\V1\\Rest\\Category\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            1 => 'api.rest.doctrine.me',
            2 => 'api.rest.doctrine.role',
            3 => 'api.rest.doctrine.user',
            4 => 'api.rest.doctrine.client',
            5 => 'api.rest.product',
            0 => 'api.rest.category',
        ),
        'default_version' => 1,
    ),
    'zf-rest' => array(
        'API\\V1\\Rest\\User\\Controller' => array(
            'listener' => 'API\\V1\\Rest\\User\\UserResource',
            'route_name' => 'api.rest.doctrine.user',
            'route_identifier_name' => 'user_id',
            'entity_identifier_name' => 'id',
            'collection_name' => 'user',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'DELETE',
                2 => 'PATCH',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(
                0 => 'company_id',
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Db\\Entity\\User',
            'collection_class' => 'API\\V1\\Rest\\User\\UserCollection',
            'service_name' => 'User',
        ),
        'API\\V1\\Rest\\Role\\Controller' => array(
            'listener' => 'API\\V1\\Rest\\Role\\RoleResource',
            'route_name' => 'api.rest.doctrine.role',
            'route_identifier_name' => 'role_id',
            'entity_identifier_name' => 'id',
            'collection_name' => 'role',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'DELETE',
                2 => 'PATCH',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Db\\Entity\\Role',
            'collection_class' => 'API\\V1\\Rest\\Role\\RoleCollection',
            'service_name' => 'Role',
        ),
        'API\\V1\\Rest\\Client\\Controller' => array(
            'listener' => 'API\\V1\\Rest\\Client\\ClientResource',
            'route_name' => 'api.rest.doctrine.client',
            'route_identifier_name' => 'client_id',
            'entity_identifier_name' => 'id',
            'collection_name' => 'client',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'ZF\\OAuth2\\Doctrine\\Entity\\Client',
            'collection_class' => 'API\\V1\\Rest\\Client\\ClientCollection',
            'service_name' => 'Client',
        ),
        'API\\V1\\Rest\\Product\\Controller' => array(
            'listener' => 'API\\V1\\Rest\\Product\\ProductResource',
            'route_name' => 'api.rest.product',
            'route_identifier_name' => 'product_id',
            'collection_name' => 'products',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'DELETE',
                2 => 'PATCH',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(
                0 => 'order',
                1 => 'field',
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'API\\V1\\Rest\\Product\\ProductEntity',
            'collection_class' => 'API\\V1\\Rest\\Product\\ProductCollection',
            'service_name' => 'Product',
        ),
        'API\\V1\\Rest\\Category\\Controller' => array(
            'listener' => 'API\\V1\\Rest\\Category\\CategoryResource',
            'route_name' => 'api.rest.category',
            'route_identifier_name' => 'category_id',
            'collection_name' => 'categories',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(
                0 => 'order',
                1 => 'field',
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'API\\V1\\Rest\\Category\\CategoryEntity',
            'collection_class' => 'API\\V1\\Rest\\Category\\CategoryCollection',
            'service_name' => 'Category',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'API\\V1\\Rest\\User\\Controller' => 'HalJson',
            'API\\V1\\Rest\\Role\\Controller' => 'HalJson',
            'API\\V1\\Rest\\Client\\Controller' => 'HalJson',
            'API\\V1\\Rest\\Product\\Controller' => 'HalJson',
            'API\\V1\\Rest\\Category\\Controller' => 'HalJson',
        ),
        'accept-whitelist' => array(
            'API\\V1\\Rest\\User\\Controller' => array(
                0 => 'application/vnd.api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'API\\V1\\Rest\\Role\\Controller' => array(
                0 => 'application/vnd.api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'API\\V1\\Rest\\Client\\Controller' => array(
                0 => 'application/vnd.api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content-type-whitelist' => array(
            'API\\V1\\Rest\\User\\Controller' => array(
                0 => 'application/json',
            ),
            'API\\V1\\Rest\\Role\\Controller' => array(
                0 => 'application/json',
            ),
            'API\\V1\\Rest\\Client\\Controller' => array(
                0 => 'application/json',
            ),
        ),
        'accept_whitelist' => array(
            'API\\V1\\Rest\\Product\\Controller' => array(
                0 => 'application/vnd.api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'API\\V1\\Rest\\Category\\Controller' => array(
                0 => 'application/vnd.api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'API\\V1\\Rest\\Product\\Controller' => array(
                0 => 'application/vnd.api.v1+json',
                1 => 'application/json',
            ),
            'API\\V1\\Rest\\Category\\Controller' => array(
                0 => 'application/vnd.api.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Db\\Entity\\User' => array(
                'route_identifier_name' => 'user_id',
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctrine.user',
                'hydrator' => 'API\\V1\\Rest\\User\\UserHydrator',
                'max_depth' => 2,
            ),
            'API\\V1\\Rest\\User\\UserCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctrine.user',
                'is_collection' => true,
                'max_depth' => 0,
            ),
            'Db\\Entity\\Role' => array(
                'route_identifier_name' => 'role_id',
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctrine.role',
                'hydrator' => 'API\\V1\\Rest\\Role\\RoleHydrator',
            ),
            'API\\V1\\Rest\\Role\\RoleCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctrine.role',
                'is_collection' => true,
            ),
            'ZF\\OAuth2\\Doctrine\\Entity\\Client' => array(
                'route_identifier_name' => 'client_id',
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctrine.client',
                'hydrator' => 'API\\V1\\Rest\\Client\\ClientHydrator',
            ),
            'API\\V1\\Rest\\Client\\ClientCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctrine.client',
                'is_collection' => true,
            ),
            'API\\V1\\Rest\\Product\\ProductEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.product',
                'route_identifier_name' => 'product_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'API\\V1\\Rest\\Product\\ProductCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.product',
                'route_identifier_name' => 'product_id',
                'is_collection' => true,
            ),
            'API\\V1\\Rest\\Category\\CategoryEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.category',
                'route_identifier_name' => 'category_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'API\\V1\\Rest\\Category\\CategoryCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.category',
                'route_identifier_name' => 'category_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-apigility' => array(
        'db-connected' => array(),
        'doctrine-connected' => array(
            'API\\V1\\Rest\\User\\UserResource' => array(
                'object_manager' => 'doctrine.entitymanager.orm_default',
                'hydrator' => 'API\\V1\\Rest\\User\\UserHydrator',
            ),
            'API\\V1\\Rest\\Role\\RoleResource' => array(
                'object_manager' => 'doctrine.entitymanager.orm_default',
                'hydrator' => 'API\\V1\\Rest\\Role\\RoleHydrator',
            ),
            'API\\V1\\Rest\\Client\\ClientResource' => array(
                'object_manager' => 'doctrine.entitymanager.orm_default',
                'hydrator' => 'API\\V1\\Rest\\Client\\ClientHydrator',
            ),
        ),
    ),
    'zf-content-validation' => array(
        'API\\V1\\Rest\\User\\Controller' => array(
            'input_filter' => 'API\\V1\\Rest\\User\\Validator',
            'use_raw_data' => false,
            'allows_only_fields_in_filter' => true,
        ),
        'API\\V1\\Rest\\Role\\Controller' => array(
            'input_filter' => 'API\\V1\\Rest\\Role\\Validator',
            'use_raw_data' => false,
            'allows_only_fields_in_filter' => true,
        ),
        'API\\V1\\Rest\\Client\\Controller' => array(
            'input_filter' => 'API\\V1\\Rest\\Client\\Validator',
        ),
        'API\\V1\\Rest\\Product\\Controller' => array(
            'input_filter' => 'API\\V1\\Rest\\Product\\Validator',
            'query_filter' => 'API\\V1\\Rest\\Product\\QueryValidator',
        ),
        'API\\V1\\Rest\\Category\\Controller' => array(
            'input_filter' => 'API\\V1\\Rest\\Category\\Validator',
            'query_filter' => 'API\\V1\\Rest\\Category\\QueryValidator',
        ),
    ),
    'input_filter_specs' => array(
        'API\\V1\\Rest\\Company\\ValidatorUpdate' => array(
            0 => array(
                'name' => 'name',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => 255,
                        ),
                    ),
                    1 => array(
                        'name' => 'ZF\\Apigility\\Doctrine\\Server\\Validator\\NoObjectExists',
                        'options' => array(
                            'entity_class' => 'Db\\Entity\\Company',
                            'fields' => 'name',
                        ),
                    ),
                ),
                'description' => 'Company unique name',
            ),
            1 => array(
                'required' => false,
                'allow_empty' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'API\\Validator\\ObjectExists',
                        'options' => array(
                            'entity_class' => 'Db\\Entity\\User',
                            'field' => 'id',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'members',
                'description' => 'List of company member user ids(ex: ["1", "3", "6"])',
            ),
        ),
        'API\\V1\\Rest\\User\\Validator' => array(
            0 => array(
                'name' => 'email',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => 255,
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\EmailAddress',
                        'options' => array(),
                    ),
                    2 => array(
                        'name' => 'ZF\\Apigility\\Doctrine\\Server\\Validator\\NoObjectExists',
                        'options' => array(
                            'entity_class' => 'Db\\Entity\\User',
                            'fields' => 'email',
                        ),
                    ),
                ),
                'description' => 'User email',
            ),
            1 => array(
                'name' => 'firstName',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => 255,
                        ),
                    ),
                ),
                'description' => 'User first name',
            ),
            2 => array(
                'name' => 'lastName',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => 255,
                        ),
                    ),
                ),
                'description' => 'User last name',
            ),
            3 => array(
                'name' => 'status',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\InArray',
                        'options' => array(
                            'haystack' => array(
                                0 => 'active',
                                1 => 'disabled',
                            ),
                        ),
                    ),
                ),
                'description' => 'User status(active/disabled)',
            ),
            4 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '8',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'password',
                'description' => 'User password. It should have more than 8 symbols',
            ),
            5 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'API\\Validator\\ObjectExists',
                        'options' => array(
                            'entity_class' => 'Db\\Entity\\Role',
                            'field' => 'roleId',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'roles',
                'description' => 'User roles(ex: ["admin", "editor"])',
            ),
        ),
        'API\\V1\\Rest\\Role\\Validator' => array(
            0 => array(
                'name' => 'roleId',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => 255,
                        ),
                    ),
                    1 => array(
                        'name' => 'ZF\\Apigility\\Doctrine\\Server\\Validator\\NoObjectExists',
                        'options' => array(
                            'entity_class' => 'Db\\Entity\\Role',
                            'fields' => 'roleId',
                        ),
                    ),
                ),
                'description' => 'Role unique name',
            ),
            1 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'ZF\\Apigility\\Doctrine\\Server\\Validator\\ObjectExists',
                        'options' => array(
                            'entity_class' => 'Db\\Entity\\Role',
                            'fields' => 'id',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'parent',
                'description' => 'Parent role unique identifier',
            ),
        ),
        'API\\V1\\Rest\\Client\\Validator' => array(
            0 => array(
                'name' => 'clientId',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'ZF\\Apigility\\Doctrine\\Server\\Validator\\NoObjectExists',
                        'options' => array(
                            'entity_class' => 'ZF\\OAuth2\\Doctrine\\Entity\\Client',
                            'fields' => 'clientId',
                        ),
                    ),
                ),
            ),
            1 => array(
                'name' => 'secret',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '8',
                        ),
                    ),
                ),
            ),
            2 => array(
                'name' => 'redirectUri',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
            ),
            3 => array(
                'name' => 'grantType',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
            ),
        ),
        'API\\V1\\Rest\\Product\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\I18n\\Validator\\Alnum',
                        'options' => array(
                            'allowwhitespace' => true,
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'max' => '255',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'name',
            ),
            1 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\I18n\\Validator\\Alnum',
                        'options' => array(
                            'allowwhitespace' => true,
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'description',
            ),
            2 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\I18n\\Validator\\IsInt',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\GreaterThan',
                        'options' => array(),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'price',
            ),
            3 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\I18n\\Validator\\IsInt',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\GreaterThan',
                        'options' => array(
                            'min' => '1',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'quantity',
            ),
            4 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\I18n\\Validator\\IsInt',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\GreaterThan',
                        'options' => array(
                            'min' => '0',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'category',
            ),
        ),
        'API\\V1\\Rest\\Product\\QueryValidator' => array(
            0 => array(
                'name' => 'order',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringToLower',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    2 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^(?:asc|desc)$/i',
                        ),
                    ),
                ),
                'description' => 'Order by direction',
                'allow_empty' => false,
                'continue_if_empty' => false,
            ),
            1 => array(
                'name' => 'field',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringToLower',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    2 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^(?:id|name|price|quantity|category)$/',
                        ),
                    ),
                ),
                'description' => 'Order by on field',
                'allow_empty' => false,
                'continue_if_empty' => false,
            ),
        ),
        'API\\V1\\Rest\\Category\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\I18n\\Validator\\Alnum',
                        'options' => array(
                            'allowwhitespace' => true,
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'max' => '255',
                            'min' => '3',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'name' => 'name',
            ),
            1 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'name' => 'description',
            ),
            2 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\I18n\\Validator\\IsInt',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(),
                    ),
                    2 => array(
                        'name' => 'Zend\\Validator\\GreaterThan',
                        'options' => array(),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'name' => 'parent',
            ),
        ),
        'API\\V1\\Rest\\Category\\QueryValidator' => array(
            0 => array(
                'name' => 'order',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringToLower',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    2 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^(?:asc|desc)$/i',
                        ),
                    ),
                ),
                'description' => 'Order by direction',
                'allow_empty' => false,
                'continue_if_empty' => false,
            ),
            1 => array(
                'name' => 'field',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringToLower',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    2 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^(?:id|name|parent)$/',
                        ),
                    ),
                ),
                'description' => 'Order by on field',
                'allow_empty' => false,
                'continue_if_empty' => false,
            ),
        ),
    ),
    'doctrine-hydrator' => array(
        'API\\V1\\Rest\\User\\UserHydrator' => array(
            'entity_class' => 'Db\\Entity\\User',
            'object_manager' => 'doctrine.entitymanager.orm_default',
            'by_value' => true,
            'strategies' => array(
                'roles' => 'ZF\\Apigility\\Doctrine\\Server\\Hydrator\\Strategy\\CollectionExtract',
                'settings' => 'ZF\\Apigility\\Doctrine\\Server\\Hydrator\\Strategy\\CollectionExtract',
            ),
            'use_generated_hydrator' => true,
            'filters' => array(
                'exclude_name' => array(
                    'condition' => 'and',
                    'filter' => 'hydrator_filter_exclude_user_fields',
                ),
            ),
        ),
        'API\\V1\\Rest\\Role\\RoleHydrator' => array(
            'entity_class' => 'Db\\Entity\\Role',
            'object_manager' => 'doctrine.entitymanager.orm_default',
            'by_value' => true,
            'strategies' => array(),
            'use_generated_hydrator' => true,
        ),
        'API\\V1\\Rest\\Client\\ClientHydrator' => array(
            'entity_class' => 'ZF\\OAuth2\\Doctrine\\Entity\\Client',
            'object_manager' => 'doctrine.entitymanager.orm_default',
            'by_value' => true,
            'strategies' => array(
                'scope' => 'ZF\\Apigility\\Doctrine\\Server\\Hydrator\\Strategy\\CollectionExtract',
            ),
            'filters' => array(
                'exclude_name' => array(
                    'condition' => 'and',
                    'filter' => 'hydrator_filter_exclude_client_fields',
                ),
            ),
            'use_generated_hydrator' => true,
        ),
    ),
    'zf-mvc-auth' => array(
        'authentication' => array(
            'map' => array(
                'API\\V1' => 'oauth2_doctrine',
            ),
            'types' => array(
                0 => 'token',
            ),
            'adapters' => array(
                'oauth2_doctrine' => array(
                    'adapter' => 'ZF\\MvcAuth\\Authentication\\OAuth2Adapter',
                    'storage' => array(
                        'storage' => 'oauth2.doctrineadapter.default',
                        'route' => '/oauth',
                    ),
                ),
            ),
        ),
        'authorization' => array(
            'deny_by_default' => false,
            'API\\V1\\Rest\\User\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'API\\V1\\Rest\\Role\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'API\\V1\\Rest\\Client\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'API\\V1\\Rest\\Product\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'API\\V1\\Rest\\Category\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'invokables' => array(
            'hydrator_filter_exclude_user_fields' => 'API\\Hydrator\\Filter\\ExcludeUserFields',
            'hydrator_filter_exclude_client_fields' => 'API\\Hydrator\\Filter\\ExcludeClientFields',
            'hydrator_filter_exclude_scope_fields' => 'API\\Hydrator\\Filter\\ExcludeScopeFields',
            'role_render_listener' => 'API\\Listener\\RoleRenderListener',
            'user_listener' => 'API\\Listener\\UserResourceListener',
        ),
        'aliases' => array(
            'ZF\\OAuth2\\Provider\\UserId' => 'ZF\\OAuth2\\Provider\\UserId\\AuthenticationService',
            'translator' => 'MvcTranslator',
        ),
        'factories' => array(
            'ZF\\OAuth2\\Provider\\UserId\\AuthenticationService' => 'API\\OAuth2\\Provider\\UserId\\AuthenticationServiceFactory',
            'ZF\\Apigility\\Documentation\\ApiFactory' => 'API\\Documentation\\Factory\\ApiFactory',
            'API\\V1\\Rest\\Product\\ProductResource' => 'API\\V1\\Rest\\Product\\ProductResourceFactory',
            'API\\V1\\Rest\\Category\\CategoryResource' => 'API\\V1\\Rest\\Category\\CategoryResourceFactory',
            'API\\Listener\\QueryValidationListener' => 'API\\Listener\\QueryValidationListenerFactory',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            0 => array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
                'text_domain' => '',
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'zf-apigility-documentation-swagger/list' => __DIR__ . '/../view/swagger/list.phtml',
        ),
    ),
);
