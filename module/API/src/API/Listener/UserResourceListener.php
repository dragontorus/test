<?php

namespace API\Listener;


use Zend\EventManager\SharedEventManagerInterface;
use Zend\EventManager\SharedListenerAggregateInterface;
use ZF\Apigility\Doctrine\Server\Event\DoctrineResourceEvent;

class UserResourceListener implements SharedListenerAggregateInterface
{
    protected $listeners;

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the SharedEventManager
     * implementation will pass this to the aggregate.
     *
     * @param SharedEventManagerInterface $events
     */
    public function attachShared(SharedEventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(
            'ZF\Apigility\Doctrine\DoctrineResource',
            DoctrineResourceEvent::EVENT_CREATE_PRE,
            [$this, 'onPreSave']
        );

        $this->listeners[] = $events->attach(
            'ZF\Apigility\Doctrine\DoctrineResource',
            DoctrineResourceEvent::EVENT_PATCH_PRE,
            [$this, 'onPreSave']
        );
    }

    /**
     * Detach all previously attached listeners
     *
     * @param SharedEventManagerInterface $events
     */
    public function detachShared(SharedEventManagerInterface $events)
    {
        foreach($this->listeners as $listener){
            $events->detach($listener);
        }
    }

    public function onPreSave(DoctrineResourceEvent $e) {

        if(!($e->getEntity() instanceof \Db\Entity\User)) {
            return;
        }

        $data = $e->getData();

        if(property_exists($data, 'password')) {
            /**
             * @var $userService \Db\Service\User
             */
            $userService = $e->getTarget()->getServiceManager()->get('Db\Service\User');

            $data->salt = $userService->generateSalt();
            $data->password = $userService->encryptPassword($data->password, $data->salt);
        }

        if(property_exists($data, 'roles')) {
            /**
             * @var $userEntity \Db\Entity\User
             */
            $userEntity = $e->getEntity();

            $userEntity->getRoles()->clear();

            if(is_array($data->roles) && !empty($data->roles)) {

                $roleList = $e->getObjectManager()->getRepository('Db\Entity\Role')->findBy(['roleId' => $e->getData()->roles]);
                foreach ($roleList as $role) {
                    $userEntity->getRoles()->add($role);
                }
            }
        }

    }

}