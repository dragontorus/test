<?php

namespace API\Validator;


use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\Validator\Exception;
use ZF\Apigility\Admin\InputFilter\Validator\AbstractValidator;

class ObjectExists extends AbstractValidator implements ServiceLocatorAwareInterface
{

    /**
     * Error constants
     */
    const ERROR_NO_OBJECT_FOUND = 'noObjectFound';

    /**
     * @var array Message templates
     */
    protected $messageTemplates = array(
        self::ERROR_NO_OBJECT_FOUND => "No object matching '%value%' was found",
    );

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        $values = [];

        if(!is_array($value)) {
            $values[] = $value;
        } else {
            $values = $value;
        }

        $validator = new \DoctrineModule\Validator\ObjectExists(array(
            'object_repository' => $this->getServiceLocator()->getServiceLocator()->get('Doctrine\ORM\EntityManager')->getRepository($this->getOption('entity_class')),
            'fields' => array($this->getOption('field'))
        ));



        foreach($values as $value){

            if(!$validator->isValid($value)){
                foreach($validator->getMessages() as $key => $message) {
                    $this->error($key, $message);
                }
                return false;
            }

        }

        return true;
    }

    /**
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }


}