<?php

namespace API\OAuth2\Provider\UserId;

use Zend\Authentication\AuthenticationService as ZendAuthenticationService;
use Zend\Stdlib\RequestInterface;
use ZF\OAuth2\Provider\UserId\UserIdProviderInterface;

/**
 * Basically this class is the same as ZF\OAuth2\Provider\UserId\AuthenticationService
 * It has some bug fixes
 *
 *
 * Class AuthenticationService
 * @package API\OAuth2\Provider\UserId
 *
 */
class AuthenticationService implements UserIdProviderInterface
{
    /**
     * @var ZendAuthenticationService
     */
    private $authenticationService;

    /**
     * @var string
     */
    private $userId = 'id';

    /**
     *  Set authentication service
     *
     * @param ZendAuthenticationService $service
     * @param array $config
     */
    public function __construct(ZendAuthenticationService $service = null, $config = [])
    {
        $this->authenticationService = $service;

        if (isset($config['zf-oauth2']['user_id'])) {
            $this->userId = $config['zf-oauth2']['user_id'];
        }
    }

    /**
     * Use Zend\Authentication\AuthenticationService to fetch the identity.
     *
     * @param  RequestInterface $request
     * @return mixed
     */
    public function __invoke(RequestInterface $request)
    {
        if (empty($this->authenticationService)) {
            return null;
        }

        /**
         * @var $identity \Db\Entity\User
         */
        $identity = $this->authenticationService->getIdentity();

        if (is_object($identity)) {

            $method = "get" . ucfirst($this->userId);
            if (method_exists($identity, $method)) {
                return $identity->$method();
            }

            return null;
        }

        if (is_array($identity) && isset($identity[$this->userId])) {
            return $identity[$this->userId];
        }

        return null;
    }

}
