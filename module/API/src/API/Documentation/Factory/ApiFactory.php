<?php


namespace API\Documentation\Factory;


use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ApiFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return \API\Documentation\ApiFactory
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new \API\Documentation\ApiFactory(
            $serviceLocator->get('ModuleManager'),
            $serviceLocator->get('Config'),
            $serviceLocator->get('ZF\Configuration\ModuleUtils')
        );
    }

}