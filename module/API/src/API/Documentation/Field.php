<?php


namespace API\Documentation;

class Field extends \ZF\Apigility\Documentation\Field
{
    protected $type;



    /**
     * Cast object to array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'description' => $this->description,
            'required' => $this->required,
            'type' => $this->type,
        ];
    }


}