<?php


namespace Db\DBAL\Type;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class UserStatus extends Type
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLED = 'disabled';

    public function getName(){
        return 'enum_user_status';
    }

    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param array $fieldDeclaration The field declaration.
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform The currently used database platform.
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
        return "ENUM('active', 'disabled') COMMENT '(DC2Type:enum_user_status)'";
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!in_array($value, array(self::STATUS_ACTIVE, self::STATUS_DISABLED))) {
            throw new \InvalidArgumentException("Invalid status");
        }
        return $value;
    }

}