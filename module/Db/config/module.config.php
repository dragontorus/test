<?php
return [
    'doctrine' => [
        'driver' => [
            'db_driver' => [
                'class' => '\\Doctrine\\ORM\\Mapping\\Driver\\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    0 => __DIR__ . '/../src/Db/Entity/',
                ],
            ],
            'orm_default' => [
                'drivers' => [
                    'Db\\Entity\\' => 'db_driver',
                ],
            ],
        ],

        'connection' => [
            'orm_default' => [
                'doctrine_type_mappings' => [
                    'enum' => 'string',
                ],
            ],
        ],

        'configuration' => [
            'orm_default' => [
                'types' => [
                    'enum_user_status' => 'Db\DBAL\Type\UserStatus',
                ],
                'proxy_dir' => __DIR__ . '/../../../data/DoctrineORMModule/Proxy',
                'proxy_namespace' => 'DoctrineORMModule\Proxy',
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            'Db\Service\User' => 'Db\Service\Factory\User',
            'Db\Service\Product' => 'Db\Service\Factory\Product',
            'Db\Service\Category' => 'Db\Service\Factory\Category',
        ]
    ]
];